package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ThreadControllerTests {
    ThreadController threadController;
    User user;
    Post post;
    ArrayList<Post> posts;
    Thread thread;
    ArrayList<Thread> threads;

    int threadId = 1;
    String forum = "sqr-forum";
    String slug = "sqr";
    String nickname = "Name";

    @BeforeEach
    void setUp() {
        threadController = new ThreadController();
        Timestamp timestamp = new Timestamp(12345678);
        user = new User(nickname, "name@example.mail", "Name Secondname", "Hello, I am Name Secondname.");

        post = new Post(nickname, timestamp, forum, "Hi", 0, 0, false);
        posts = new ArrayList<>();
        posts.add(post);

        thread = new Thread(nickname, timestamp, forum, "Hello", slug, "SQR", 0);
        thread.setId(threadId);
        threads = new ArrayList<>();
        threads.add(thread);
    }

    @Test
    @DisplayName("Test. CreatePost method")
    public void testCreatePost() {
        try (MockedStatic<UserDAO> mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
            try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                mockedUserDAO.when(() -> UserDAO.Info(nickname)).thenReturn(user);
                mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(posts), threadController.createPost(slug, posts));
                assertEquals(thread, ThreadDAO.getThreadBySlug(slug));
            }
        }
    }

    @Test
    @DisplayName("Test. Posts method")
    public void testPosts() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getPosts(threadId, 1, 0, "tree", false)).thenReturn(posts);
            ResponseEntity responseEntity = threadController.Posts(threadId + "", 1, 0, "tree", false);
            assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
            assertEquals(posts, responseEntity.getBody());
        }
    }

    @Test
    @DisplayName("Test. CheckIdOrSlug method")
    public void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            assertEquals(thread, threadController.CheckIdOrSlug(threadId + ""));
            assertEquals(thread, threadController.CheckIdOrSlug(slug));
        }
    }

    @Test
    @DisplayName("Test. Change method")
    public void testChange() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            Thread newThread = new Thread("Ivan", new Timestamp(777), "...", "smt", "mlem", "High quality forum", 3);
            newThread.setId(3);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(3)).thenReturn(newThread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("mlem")).thenReturn(newThread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.change(thread, newThread)).thenAnswer((invocationOnMock) -> {
                mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug("mlem")).thenReturn(newThread);
                return null;
            });
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(newThread), threadController.change("mlem", newThread));
            assertEquals(newThread, threadController.CheckIdOrSlug("mlem"));
        }
    }

    @Test
    @DisplayName("Test. Info method")
    public void testInfo() {
        try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
            mockedThreadDAO.when(() -> ThreadDAO.getThreadById(threadId)).thenReturn(thread);
            mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), threadController.info(threadId + ""));
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), threadController.info(slug));
        }
    }

    @Test
    @DisplayName("Test. CreateVote method")
    public void testCreateVote() {
        try (MockedStatic<UserDAO> mockedUserDAO = Mockito.mockStatic(UserDAO.class)) {
            try (MockedStatic<ThreadDAO> mockedThreadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                mockedUserDAO.when(() -> UserDAO.Info(nickname)).thenReturn(user);
                Vote vote = new Vote(nickname, 1);
                mockedThreadDAO.when(() -> ThreadDAO.change(vote, thread.getVotes())).thenReturn(thread.getVotes());
                mockedThreadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), threadController.createVote(slug, vote));
            }
        }
    }
}
